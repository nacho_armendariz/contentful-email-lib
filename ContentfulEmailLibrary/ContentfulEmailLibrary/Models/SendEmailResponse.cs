﻿namespace ContentfulEmailLibrary.Models
{
    public class ResponseWithMessage
    {
        public ResponseMessage ResponseMessage { get; set; }
    }

    public class ResponseMessage
    {
        public int? MessageId { get; set; }
        public string Message { get; set; }
        public string FriendlyMessage { get; set; }
        public string MessageType { get; set; }
        public string StackTrace { get; set; }
    }

    public class SendEmailResponse : ResponseWithMessage
    {
        public bool Success { get { return !string.IsNullOrEmpty(Result); } }
        public string Result { get; set; }
    }
}
