﻿using ContentfulEmailLibrary.Models;
using Equinox.Libraries.Tracing;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace ContentfulEmailLibrary.Service
{
    public interface IContentfulEmailService
    {
        SendEmailResponse SendEmail(string templateName, object dataContent, string recipients, string sourceApp, string firstName, string memberId);
    }

    public class ContentfulEmailService : IContentfulEmailService
    {
        private ITrace _trace;
        private string _hostURL;

        public ContentfulEmailService(ITrace trace, string hostURL)
        {
            _trace = trace;
            _hostURL = hostURL;
        }

        public HttpResponseMessage SendRequest(string url)
        {
            var apiUrl = _hostURL + url;
            try
            {
                using (var _httpClient = new HttpClient())
                {
                    _httpClient.DefaultRequestHeaders.Accept.Clear();
                    _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //_httpClient.DefaultRequestHeaders.Add("apikey", apiKey);
                    return _httpClient.GetAsync(apiUrl).Result;
                }
            }
            catch (Exception ex)
            {
                _trace.Information("Error occurred in ContentfulEmailService: " + ex.Message + " - URL: " + apiUrl);
                _trace.Error(ex);
                return new HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public HttpResponseMessage SendRequest(string url, string content, HttpMethod httpMethod)
        {
            var apiUrl = _hostURL + url;
            try
            {
                var request = new HttpRequestMessage(httpMethod, apiUrl)
                {
                    Content = new StringContent(content, Encoding.UTF8, "application/json"),
                };
                using (var _httpClient = new HttpClient())
                {
                    _httpClient.DefaultRequestHeaders.Accept.Clear();
                    _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //_httpClient.DefaultRequestHeaders.Add("apikey", apiKey);
                    return _httpClient.SendAsync(request).Result;
                }
            }
            catch (Exception ex)
            {
                _trace.Information("Error occurred in ContentfulEmailService: " + ex.Message + " - URL: " + apiUrl.ToString());
                _trace.Error(ex);
                return new HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public SendEmailResponse SendEmail(string templateName, object dataContent, string recipients, string sourceApp, string firstName, string memberId)
        {
            var postData = new {
                template = templateName,
                data = dataContent,
                recipients = recipients,
                firstName = firstName,
                memberID = memberId,
                sourceApp = sourceApp,
                splitRecipients = true
            };
            var response = SendRequest("sendEmail", JsonConvert.SerializeObject(postData), HttpMethod.Post);
            return response.IsSuccessStatusCode ? JsonConvert.DeserializeObject<SendEmailResponse>(response.Content.ReadAsStringAsync().Result) : null;
        }
    }
}
